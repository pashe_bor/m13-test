var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', express.static('./dist'));
app.use('/bucket', express.static('./dist'));
app.use('/graph', express.static('./dist'));
app.use('/', express.static('./data'));


function saveEditedArticle(editedArt, callback) {
    var jsonToUptade = JSON.parse(fs.readFileSync('./app/assets/js/articles.json', 'utf8'));

    jsonToUptade.forEach(function(item) {
        var itemId = item.id, editedItem = editedArt.id;
        if(itemId == editedItem){
            item.author = editedArt.author;
            item.header = editedArt.header;
            item.text = editedArt.text;
            item.tags = editedArt.tags;
            item.image = editedArt.image;
            item.time = editedArt.time;
        }
    });

    fs.writeFile('./app/assets/js/articles.json', JSON.stringify(jsonToUptade, null, 4), callback);
}


app.post('/delete_article', function(req, res) {
    res.json(req.body);

    deleteArticle(req.body, function(error) {
        if (error) {
            res.status(404).send('The record did not deleted');
            return;
        }
    });

});

app.get('/graphs', function(req, res) {
    var json = JSON.parse(fs.readFileSync('./data/graph.json', 'utf8'));
    res.send(json);
});


app.listen(3000, function () {
    console.log('Test app is listening on port 3000!');
});