import {
    ADD_MAIL_TO_BUCKET, DELETE_MAIL_FROM_BUCKET, FILTER_TYPE_BUCKET
} from '../actions/constants';

const initialState = {
    bucketState: [],
    bucketTypeSort: ''
};

export const bucketReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MAIL_TO_BUCKET:
            return Object.assign({}, state, {bucketState: [...state.bucketState, action.payload] })
        case FILTER_TYPE_BUCKET:
            return Object.assign({}, state, {bucketTypeSort: action.payload})
        case DELETE_MAIL_FROM_BUCKET:
            return Object.assign({}, state, {bucketState: state.bucketState.filter(item => item.id !== action.payload)})
    }
    return state;
};