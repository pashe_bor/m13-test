import {FILTER_TYPE_GRAPH, SET_GRAPH_NODES} from '../actions/constants';

const initialState = {
    graphs: null,
    graphTypeSort: ''
};

export const graphsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_GRAPH_NODES:
            return Object.assign({}, state, {graphs:  action.payload})
        case FILTER_TYPE_GRAPH:
            return Object.assign({}, state, {graphTypeSort: action.payload})
    }
    return state;
};