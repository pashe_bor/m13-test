import {FILTER_SEARCH, FILTER_TYPE_SEARCH} from '../actions/constants';

const initialState = {
    searchState: {
        word: '',
        type: ''
    }
};

export const searchReducer = (state = initialState, action) => {
    switch (action.type) {
        case FILTER_SEARCH:
            return Object.assign({}, state, {
                searchState: Object.assign({}, state.searchState, {
                    word: action.payload
                })
            });
         case FILTER_TYPE_SEARCH:
             return Object.assign({}, state, {
                 searchState: Object.assign({}, state.searchState, {
                     type: action.payload
                 })
             });
    }
    return state;
};