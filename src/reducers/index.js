import { combineReducers } from 'redux';

import {mainReducer} from './main.reducer';
import {searchReducer} from './search.reducer';
import {bucketReducer} from './bucket.reducer';
import {graphsReducer} from './graphs.reducer';


const reducers = combineReducers({
    mainReducer: mainReducer,
    searchReducer: searchReducer,
    bucketStore: bucketReducer,
    graphsStore: graphsReducer
});

export default reducers;