import {GET_GRAPH_DATA, NOTIFICATION_SHOW, SHOW_POPUP} from '../actions/constants';

const initialState = {
    graphsData: '',
    isPopupShown: false,
    notificationData: {
        isShow: false,
        itemId: ''
    }
};

export const mainReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_GRAPH_DATA:
            return Object.assign({}, state, {graphsData: action.payload});
        case NOTIFICATION_SHOW:
            return Object.assign({}, state, {notificationData: action.payload});
        case SHOW_POPUP:
            return Object.assign({}, state, {isPopupShown: action.payload})
    }
    return state;
};