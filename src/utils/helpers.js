//Finding all user relations
export const findAllRelations = (entity, graph, entities) => {
    const dataRelations = graph.filter(item => {
        if(item.source === entity.id || item.target === entity.id) {
            return item;
        }
    }), dataEnteties = dataRelations.map(entitySearch => {
        if(entitySearch.source !== entity.id) {
            return {label: entitySearch.label, id: entitySearch.source}
        } else if(entitySearch.target !== entity.id) {
            return {label: entitySearch.label, id: entitySearch.target}
        }
    }), relativeEntities = dataEnteties.map(item => {
        return {type: item.label , relative_entity: entities.find(mail => {
            if (mail.id === item.id) {
                return mail;
            }
        })}
    });

    return relativeEntities;
}

export const findNodesAndEdges = (entity, edges, nodes) => {
    let viewData = {
        nodes: [],
        edges: []
    }, relativeEntities = null;
    //Finding all edges of selected graph nodes
    for (let i = 0; i < entity.length; i++) {
        viewData.nodes.push(entity[i]);

        const dataRelations = edges.filter(item => {
            if(item.source === entity[i].id || item.target === entity[i].id) {
                viewData.edges.push(item);
                return item;
            }
        }), dataEntities = dataRelations.map(entitySearch => {
            if(entitySearch.source !== entity[i].id) {
                return {label: entitySearch.label, id: entitySearch.source}
            } else if(entitySearch.target !== entity[i].id) {
                return {label: entitySearch.label, id: entitySearch.target}
            }
        });

        dataEntities.map(item => {
            return nodes.find(mail => {
                    if (mail.id === item.id) {
                        viewData.nodes.push(mail);
                    }
                })
        });
    }

    return viewData;
}

//Getting all types from edges
export const getEdgesTypes = (graph) => {
    const getLabels = graph.map(item => {
        return item.label;
    }), filteredLabels = getLabels.filter((label, pos, array) => {
        return array.indexOf(label) === pos;
    })
    return filteredLabels
}