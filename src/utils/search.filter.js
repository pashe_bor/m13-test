export const searchFilter = (state, filterState, type = null)=> {
    let filteredState = state.filter( (element) => {
        if (!type) {
            return (element.id.indexOf(filterState) !== -1) || (element.id == filterState)
                || (element.label.toLowerCase().indexOf(filterState.toLowerCase()) !== -1)
                || (element.summary.toLowerCase().replace(/\s/g, '').indexOf(filterState.toLowerCase().replace(/\s/g, '')) !== -1);
        } else {
            return ((element.id.indexOf(filterState) !== -1) || (element.id == filterState)
                || (element.summary.toLowerCase().replace(/\s/g, '').indexOf(filterState.toLowerCase().replace(/\s/g, '')) !== -1))
                && (element.label.toLowerCase().indexOf(type.toLowerCase()) !== -1);
        }
    });
    return filteredState;
};