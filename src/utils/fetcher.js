export const httpGet = (url) => {
    return fetch(url)
        .then(response => response.json())
};