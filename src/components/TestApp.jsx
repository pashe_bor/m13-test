import React from 'react';
import GraphsView from './graphs-view/GraphsView';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import SearchScreen from "./search-screen/SearchScreen.jsx";
import BucketScreen from "./bucket/BucketScreen";
import {bindActionCreators} from "redux";
import {getGraphs} from "../actions/actions";
import {connect} from "react-redux";

class TestApp extends React.Component{

    componentDidMount() {
        if(!this.props.graphsData) {
            this.props.getGraphs();
        }
    }
    render() {
        return(
            <main>
                <BrowserRouter>
                    <Switch>
                        <Route exact path={'/'} component={SearchScreen}/>
                        <Route path={'/bucket'} component={BucketScreen}/>
                        <Route path={'/graph'} component={GraphsView}/>
                    </Switch>
                </BrowserRouter>
            </main>
        )
    }
}

export function mapStateToProps(store) {
    return {
        graphsData: store.mainReducer.graphsData
    }
}

export const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({getGraphs}, dispatch)
};

export  default  connect(mapStateToProps, mapDispatchToProps)(TestApp);