import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import {bucketTypeAction, deleteMailFromBucket, filterGraph, showPopup} from "../../actions/actions";
import DefaultButton from "../elements/buttons/DefaultButton";
import Select from "../elements/select/Select";
import SearchButton from "../elements/buttons/SearchButton";
import {findNodesAndEdges} from "../../utils/helpers";
import { InteractiveForceGraph, ForceGraphNode, ForceGraphLink } from 'react-vis-force';
import {searchFilter} from "../../utils/search.filter";
import BucketButton from "../elements/buttons/BucketButton";
import {Link} from "react-router-dom";


class GraphsView extends Component {
    constructor(props) {
        super(props);
        this.state = {type: 'not selected', popupData: {entity: {}, relations: {}}};
    }

    check(value) {
        console.log(value)
    }

    searchButtonClickHandler() {
        this.props.history.push('/')
    }

    backToBucketHandler() {
        this.props.history.push('/bucket')
    }

    //Labels for a 'Select' button
    getLabels() {
        let labels = this.props.graphs.map(item => item.label);

        return labels.filter((label, pos, array) => {
            return array.indexOf(label) === pos;
        });
    }

    selectItemChooseEvent(event) {
        this.props.filterGraph(event.target.getAttribute('itemvalue'));
        this.setState({type: event.target.innerText})
    }

    render() {
        if(!this.props.graphs) {
            return(
                <main className={'graphs-screen container'}>
                    <h1 className={'title '}>Graphs</h1>
                    <h5 style={{textAlign: 'center'}}>No selected data, proceed to Search page</h5>
                    <Link to={'/'}
                          style={{textAlign: 'center', display: 'block'}}>go to Search page</Link>
                </main>
            )
        } else {
            const viewData = findNodesAndEdges(
                searchFilter(this.props.graphs, '', this.props.graphTypeSort),
                this.props.graphsData.subgraph.edges,
                this.props.graphsData.subgraph.vertices
            );
            const colors = (label) => {
                switch (label) {
                    case 'Email':
                        return 'blue'
                    case 'EmailAddress':
                        return 'green'
                    case 'Alias':
                        return 'yellow'
                    case 'Document':
                        return 'black'
                }
            }
            return(
                <main className={'graphs-screen container'}>
                    <h1 className={'title '}>Graphs</h1>
                    <nav className={'nav-panel'}>
                        <Select type={this.state.type} list={this.getLabels()} itemClick={this.selectItemChooseEvent.bind(this)}/>
                        <BucketButton  onClick={this.backToBucketHandler.bind(this)}/>
                        <SearchButton onClick={this.searchButtonClickHandler.bind(this)}/>
                    </nav>
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                        <InteractiveForceGraph
                            simulationOptions={{animate: true, radiusMargin: 20000}}
                            zoom
                            labelAttr="label"
                        >
                            {viewData.nodes.map((node, i) => <ForceGraphNode onClick={this.check.bind(node)} key={i} node={{
                                id: node.id,
                                label: '#' + node.id + ' ' + node.summary}}  fill={colors(node.label)} />)}
                            {viewData.edges.map((edge, i ) => <ForceGraphLink strength={100} key={i} link={{label: edge.label, source: edge.source, target: edge.target }} />)}
                        </InteractiveForceGraph>
                    </div>
                </main>
            )
        }
    }
}

export function mapStateToProps(store) {
    return {
        graphs: store.graphsStore.graphs,
        graphsData: store.mainReducer.graphsData,
        graphTypeSort: store.graphsStore.graphTypeSort
    }
}

export const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({filterGraph}, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(GraphsView);