import React from 'react';

const Popup = ({onClick, data}) => {
    return (
        <section className={'popup-overlay'} onClick={onClick}>
            <div className="popup">
                <p className={'popup__close'}>&times;</p>
                <div className="popup-body">
                    <h3 className={'popup__title'}>Entry #{data.entity.id}</h3>
                    <div className={'popup-body-info'}>
                        <h4 className={'popup-body-info__type'}>Type: {data.entity.label}</h4>
                        <p className={'popup-body-info__message'}><b>Summary:</b> {data.entity.summary}</p>
                        <label><b>Relations:</b></label>
                        <div className={'popup-body-info-relations'}>
                            {
                                data.relations.map((entity, i) => {
                                    return (
                                        <div className={'popup-body-info-relations__entity'} key={i}>
                                            <h4>{entity.type} {entity.relative_entity.label} (#{entity.relative_entity.id})</h4>
                                            <p><b>Summary:</b> {entity.relative_entity.summary}</p>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Popup;