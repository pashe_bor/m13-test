import React from 'react';
import {Debounce, Throttle} from 'react-throttle';

const SearchField = (props) =>{
        return (
            <div className={'search-field'}>
                    <input type={'text'}
                           placeholder={'ID, Label, Summary...'}
                           onChange={props.onChange}
                    />
            </div>
        )
}

export default SearchField;