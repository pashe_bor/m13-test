import React from 'react';

const SearchButton = (props) => {
    return(
        <button className={'button-search'} onClick={props.onClick}></button>
    )
};

export default SearchButton;