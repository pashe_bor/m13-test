import React from 'react';

const DefaultButton = ({onClick, buttonLabel}) => {
    return(
        <button type={'button'} onClick={onClick} className={'button'}>{buttonLabel}</button>
    )
};

export default DefaultButton;