import React from 'react';

class Select extends React.Component{
    constructor(props) {
        super(props);
        this.state = {isOpen: false, vertices: null}
    }

    selectClickEvent () {
        this.state.isOpen ? this.setState({isOpen: false}) : this.setState({isOpen: true})
    }

    render() {
        const listElements = this.props.list;

        return(
            <section className={'select'}>
                <button className={`select-button ${this.state.isOpen ? 'select-button--opened' : 'select-button--closed'}`}
                        type={'button'}
                        onClick={this.selectClickEvent.bind(this)}>Type: {this.props.type}</button>
                <ul className={`select-list ${this.state.isOpen ? 'select-list--open' : null }`}>
                    <li className={'select-list__item'} itemvalue={''} onClick={this.props.itemClick}>not selected</li>
                    {listElements.map((item, i) => <li className={'select-list__item'}
                                                       key={i}
                                                       onClick={this.props.itemClick}
                                                       itemvalue={item}
                                                       >{item}</li>)}
                </ul>
            </section>
        )
    }
}

export default Select;