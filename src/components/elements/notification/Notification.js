import React from 'react';

const Notification = (props)  => {
    return(
        <div className={'notification'}>
            <p>Item <b>#{props.itemId}</b> has been added to the Bucket!</p>
        </div>
    )
}

export default Notification;