import React from 'react';

class BucketWidget extends React.Component{

    getItems() {
        this.props.list.map(listItem => {
            return(
                <li className={'bucket-widget-list__item'}>
                    <p>{listItem.label}</p>
                    <p>{listItem.summary}</p>
                </li>
            )
        })
    }

    render() {
        return(
            <aside className={'bucket-widget'}>
                <ul className={'bucket-widget-list'}>
                    {this.getItems()}
                </ul>
            </aside>
        )
    }
}

export default BucketWidget;