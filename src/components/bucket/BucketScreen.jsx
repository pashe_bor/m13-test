import React, {Component} from 'react';
import {
    bucketTypeAction,
    deleteMailFromBucket,
    showPopup,
    setGraphNodes
} from "../../actions/actions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import ReactTable from "react-table";
import SearchButton from "../elements/buttons/SearchButton";
import Select from "../elements/select/Select";
import {searchFilter} from "../../utils/search.filter";
import Popup from "../elements/popup/Popup";
import {findAllRelations} from "../../utils/helpers";
import DefaultButton from "../elements/buttons/DefaultButton";

class BucketScreen extends Component{

    constructor(props) {
        super(props);
        this.state = {type: 'not selected', popupData: {entity: {}, relations: {}}};
    }

    searchButtonClickHandler() {
        this.props.history.push('/')
    }

    vizualazeBtnHandler() {
        this.props.setGraphNodes(this.props.bucketState)
        this.props.history.push('/graph')
    }

    selectItemChooseEvent(event) {
        this.props.bucketTypeAction(event.target.getAttribute('itemvalue'));
        this.setState({type: event.target.innerText})
    }

    getLabels() {
        let labels = this.props.bucketState.map(item => item.label);

        return labels.filter((label, pos, array) => {
            return array.indexOf(label) === pos;
        });
    }

    deleteMail(value) {
        this.props.deleteMailFromBucket(value.original.id)
    }

    filteredData() {
        return searchFilter(
            this.props.bucketState,
            '',
            this.props.bucketTypeSort
        );
    }

    closePopupHandler(event) {
        event.preventDefault();
        if (event.target.classList.contains('popup-overlay') ||
            event.target.classList.contains('popup__close')) {

            this.props.showPopup(false)
        }
    }

    tableRowClickHandler(value){
        const relations = findAllRelations(value.original,
            this.props.graphsData.subgraph.edges,
            this.props.graphsData.subgraph.vertices);
        this.setState({popupData: {
                entity: value.original,
                relations: relations}
        });
        this.props.showPopup(true)
    }

    render() {
        const columns = [{
            Header: 'ID',
            accessor: 'id',
            defaultSortDesc: false,
            sortable: true,
            Cell: (value) => (
                <div
                    className={'cell'} onClick={this.tableRowClickHandler.bind(this, value)}
                >{value.value}</div>
            )
        }, {
            Header: 'Label',
            accessor: 'label',
            defaultSortDesc: false,
            sortable: true,
            Cell: (value) => (
                <div
                    className={'cell'} onClick={this.tableRowClickHandler.bind(this, value)}
                >{value.value}</div>
            )
        },{
            Header: 'Summary',
            accessor: 'summary',
            sortable: false,
            Cell: (value) => (
                <div
                    className={'cell'} onClick={this.tableRowClickHandler.bind(this, value)}
                >{value.value}</div>
            )
        },{
            Header: 'Delete',
            Cell: (value) => (
                <button
                    type={'button'}
                    className={'button-delete'}
                    onClick={this.deleteMail.bind(this, value)}>&times;</button>
            ),
            width: 60
        }];
        return(
            <main className={`bucket-screen container`}>
                {this.props.isPopupShown ? <Popup data={this.state.popupData}
                                                  onClick={this.closePopupHandler.bind(this)}/> : null}
                <div className={`${this.props.isPopupShown ? 'blur' : ''}`}>
                <h1 className={'title'}>Bucket</h1>
                <nav className={'nav-panel'}>
                    <Select type={this.state.type}
                            list={this.getLabels()}
                            itemClick={this.selectItemChooseEvent.bind(this)}/>
                    <DefaultButton  onClick={this.vizualazeBtnHandler.bind(this)}
                                    buttonLabel={'Vizualize it'}/>
                    <SearchButton onClick={this.searchButtonClickHandler.bind(this)}/>
                </nav>
                <ReactTable
                    columns={columns}
                    data={this.filteredData()}
                    showPagination={true}
                />
                </div>
            </main>
        )
    }
}

export function mapStateToProps(store) {
    return {
        bucketState: store.bucketStore.bucketState,
        bucketTypeSort: store.bucketStore.bucketTypeSort,
        isPopupShown: store.mainReducer.isPopupShown,
        graphsData: store.mainReducer.graphsData
    }
}

export const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({bucketTypeAction, deleteMailFromBucket, showPopup, setGraphNodes}, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(BucketScreen);