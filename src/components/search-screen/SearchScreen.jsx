import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import {addMailToBucket, searchAction, searchTypeAction, showNotification} from '../../actions/actions';
import ReactTable from "react-table";
import SearchField from './../elements/search/SearchField';
import BucketButton from './../elements/buttons/BucketButton';
import {searchFilter} from "../../utils/search.filter";
import Select from "../elements/select/Select";
import Notification from "../elements/notification/Notification";

class SearchScreen extends Component{

    constructor(props) {
        super(props);
        this.getGraphs = this.props.getGraphs;
        this.state = {type: 'not selected'};
    }

    searchHandler(event) {
        this.props.searchAction(event.target.value);
    }

    tableRowClickHandler(value){
        this.props.showNotification({isShow: true, itemId: value.original.id});
        this.props.addMailToBucket(value.original);
        setTimeout(() => {
            this.props.showNotification({isShow: false, itemId: ''})
        }, 3000)
    }

    bucketButtonClickHandler() {
        this.props.history.push('bucket')
    }

    selectItemChooseEvent(event) {
        this.props.searchTypeAction(event.target.getAttribute('itemvalue'));
        this.setState({type: event.target.innerText})
    }

    getLabels() {
        let labels = this.props.graphsData.subgraph.vertices.map(item => item.label);

        return labels.filter((label, pos, array) => {
            return array.indexOf(label) === pos;
        });
    }

    filteredData() {
        this.props.graphsData.subgraph.vertices.forEach(item => {
           if (!item.summary) {
               item.summary = 'none';
           }
        });
        return searchFilter(
            this.props.graphsData.subgraph.vertices,
            this.props.searchState.word,
            this.props.searchState.type
        );
    }

    showTable() {
        if(!this.props.graphsData) {
            return (<p className={'loading'}>loading...</p>)
        } else {
            const columns = [{
                    Header: 'ID',
                    accessor: 'id',
                    defaultSortDesc: false,
                    sortable: true,
                    Cell: (value) => (
                        <div
                           className={'cell'} onClick={this.tableRowClickHandler.bind(this, value)}
                        >{value.value}</div>
                    )
                }, {
                    Header: 'Label',
                    accessor: 'label',
                    defaultSortDesc: false,
                    sortable: true,
                    Cell: (value) => (
                        <div
                            className={'cell'} onClick={this.tableRowClickHandler.bind(this, value)}
                        >{value.value}</div>
                    )
                },{
                    Header: 'Summary',
                    accessor: 'summary',
                    sortable: false,
                    Cell: (value) => (
                        <div
                            className={'cell'} onClick={this.tableRowClickHandler.bind(this, value)}
                        >{value.value}</div>
                     )
                }];

            return(
                <section>
                    {this.props.notificationState.isShow ? <Notification itemId={this.props.notificationState.itemId}/> : null}
                    <nav className={'nav-panel'}>
                        <Select type={this.state.type} list={this.getLabels()} itemClick={this.selectItemChooseEvent.bind(this)}/>
                        <SearchField onChange={this.searchHandler.bind(this)}/>
                        <BucketButton onClick={this.bucketButtonClickHandler.bind(this)}/>
                    </nav>
                    <ReactTable
                        columns={columns}
                        data={this.filteredData()}
                        showPagination={true}
                    />
                </section>
            )
        }
    }

    render() {

        return(
            <main className={'search-screen container'}>
                <h1 className={'title'}>Search</h1>
                {this.showTable()}
            </main>

        )
    }
}

export function mapStateToProps(store) {
    return {
        graphsData: store.mainReducer.graphsData,
        searchState: store.searchReducer.searchState,
        notificationState: store.mainReducer.notificationData
    }
}

export const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        searchAction, searchTypeAction, showNotification, addMailToBucket
    }, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);

