import {
    GET_GRAPH_DATA,
    FILTER_SEARCH,
    FILTER_TYPE_SEARCH,
    NOTIFICATION_SHOW,
    DELETE_MAIL_FROM_BUCKET,
    ADD_MAIL_TO_BUCKET,
    FILTER_TYPE_BUCKET,
    SHOW_POPUP,
    SET_GRAPH_NODES,
    FILTER_TYPE_GRAPH
} from './constants';
import {httpGet} from '../utils/fetcher';

//Data actions
export const getGraphDataSuccess = (data) => {
    return{
        type: GET_GRAPH_DATA,
        payload: data
    }
};

export const getGraphs = () => {
    return dispatch => {
        return httpGet('/graphs')
            .then(json => {
                dispatch(getGraphDataSuccess(json));
            })
    }
};

//Search actions
export const searchAction = (searchValue) => {
    return{
        type: FILTER_SEARCH,
        payload: searchValue
    }
};

export const searchTypeAction = (searchTypeValue) => {
    return{
        type: FILTER_TYPE_SEARCH,
        payload: searchTypeValue
    }
};

//Notification show
export const showNotification = (notificationData) => {
    return{
        type: NOTIFICATION_SHOW,
        payload: notificationData
    }
};

//Bucket actions
export const addMailToBucket = (mailData) => {
    return{
        type: ADD_MAIL_TO_BUCKET,
        payload: mailData
    }
};

export const deleteMailFromBucket = (mailData) => {
    return{
        type: DELETE_MAIL_FROM_BUCKET,
        payload: mailData
    }
};

export const bucketTypeAction = (value) => {
    return{
        type: FILTER_TYPE_BUCKET,
        payload: value
    }
};

//Popup actions
export const showPopup = (state) => {
    return{
        type: SHOW_POPUP,
        payload: state
    }
};

//Graph actions
export const setGraphNodes = (nodesData) => {
    return{
        type: SET_GRAPH_NODES,
        payload: nodesData
    }
}

export const filterGraph = (value) => {
    return{
        type: FILTER_TYPE_GRAPH,
        payload: value
    }
}



